# README #

StudyTime is an in-progress mobile application designed to help college students reach their educational goals.

# Screenshots #
![Screenshot_2016-09-11-15-51-42.png](https://bitbucket.org/repo/9abpX4/images/780030093-Screenshot_2016-09-11-15-51-42.png)

![Screenshot_2016-09-11-15-55-23.png](https://bitbucket.org/repo/9abpX4/images/3422769650-Screenshot_2016-09-11-15-55-23.png)

![Screenshot_2016-09-11-15-54-10.png](https://bitbucket.org/repo/9abpX4/images/2087633874-Screenshot_2016-09-11-15-54-10.png)

![Screenshot_2016-09-24-22-12-41.png](https://bitbucket.org/repo/9abpX4/images/908457872-Screenshot_2016-09-24-22-12-41.png)

![Screenshot_2016-09-24-22-13-05.png](https://bitbucket.org/repo/9abpX4/images/2116019955-Screenshot_2016-09-24-22-13-05.png)

# TO-DO: #
### Calendar ###
* Delete multiple events at a time
* Weekly view
### Flashcards ###
* Implement a flashcard system